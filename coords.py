import cv2
import argparse
import os
import xlsxwriter

def capture_coords(event,x,y,flags,param):
    global refPt
    if event == cv2.EVENT_LBUTTONDOWN:
        refPt = [x,y]




# construct the argument parser and parse the arguments
ap = argparse.ArgumentParser()
ap.add_argument("-i", "--images", required=True, help="Path to the images")
ap.add_argument("-x", "--excel", required=True, help="Path to excel file for writing")
args = vars(ap.parse_args())


directory = os.fsencode(args["images"])
excel = args["excel"]

#create column names in excel file
workbook = xlsxwriter.Workbook(excel)
worksheet = workbook.add_worksheet()
worksheet.write('A1', 'file')
worksheet.write('B1', 'width')
worksheet.write('C1', 'height')
worksheet.write('D1', 'class')
worksheet.write('F1', 'x')
worksheet.write('E1', 'y')
row = 1
for file in os.listdir(directory):
     filename = os.fsdecode(file)
     if filename.endswith(".png"):
         full_path = "".join((args["images"],"\\",filename))
         image = cv2.imread(full_path)
         clone = image.copy()
         cv2.namedWindow("images")
         cv2.setMouseCallback("images", capture_coords)
         # display the image and wait for a keypress
         # keep looping until the 'q' key is pressed
         while True:
             # display the image and wait for a keypress
             cv2.imshow("images", image)


             key = cv2.waitKey(1) & 0xFF

             # if the 'c' key is pressed, save and break from the loop
             if key == ord("c"):
                 height, width, channels = image.shape
                 className = "nerve"
                 print(refPt[0],refPt[1])
                 worksheet.write(row,0,filename)
                 worksheet.write(row,1, width)
                 worksheet.write(row,2, height)
                 worksheet.write(row,3, className)
                 worksheet.write(row,4, refPt[0])
                 worksheet.write(row,5, refPt[1])
                 row = row + 1
                 break
         continue
     else:
         continue

workbook.close()


# close all open windows
cv2.destroyAllWindows()