import tensorflow
import os
import cv2
import numpy as np
import argparse
import pandas as pd
def process_images(images_path, filename_path, convertToGray = False):
    images = []
    directory = images_path
    filenames = open(filename_path)
    filenames = filenames.readlines()

    for file in filenames:
        file = file.rstrip()
        full_path = "".join((directory, "\\", file))
        if file.endswith(".png"):
            img = cv2.imread(full_path,flags=cv2.IMREAD_UNCHANGED)

            if not convertToGray:
                images.append(img)
            else:
                gray_image = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
                images.append(gray_image)

            #
            # cv2.namedWindow("images")
            # while True:
            #     # display the image and wait for a keypress
            #     # cv2.imshow("images", cv2.imread(full_path))
            #     key = cv2.waitKey(1) & 0xFF
            #     # if the 'c' key is pressed, save and break from the loop
            #     if key == ord("c"):
            #         break
        else:
            continue

    images = np.asarray(images)

    return images


ap = argparse.ArgumentParser()

ap.add_argument("-i", "--images", required=True, help="Path to the images")
ap.add_argument("-in", "--image_names", required=True, help="Path to the text file containing image filenames")
# ap.add_argument("-li", "--loss_images", required=True, help="Path to the loss images")
ap.add_argument("-x", "--labels", required=True, help="Path to the excel file containing labels created by using coords.py")
ap.add_argument("-c", "--checkpoint", required=True, help="Path to directory for saved_model.pb file")

args = vars(ap.parse_args())

images_dir = args["images"]
image_names= args["image_names"]
model_dir = args["checkpoint"]
model= tensorflow.keras.models.load_model(model_dir)
val_images = process_images(images_dir,image_names)
val_images = val_images/255.0

labels = args['labels']
val_data = pd.read_csv(labels,delimiter=',')
val_data = val_data.set_index('file')
ordered_val_data = np.empty((0,2))
filenames = open(image_names)
filenames = filenames.readlines()


for file in filenames:
    file = file.rstrip()
    y = val_data.loc[file,'y']
    x = val_data.loc[file,'x']
    coordinates = np.asarray([[y,x]])

    ordered_val_data = np.append(ordered_val_data,coordinates,axis=0)


loss, acc = model.evaluate(val_images, ordered_val_data, verbose=2)
print('Restored model, accuracy: {:5.2f}%'.format(100*acc))
coords = model.predict(val_images)



coordIndex = 0
directory = images_dir
for file in os.listdir(directory):

    filename = os.fsdecode(file)
    full_path = "".join((directory, "\\", filename))
    if filename.endswith(".png"):
        image = cv2.imread(full_path)
        x = int(coords[coordIndex][1])
        y = int(coords[coordIndex][0])
        # xmax = int(coords[coordIndex][2])
        # ymax = int(coords[coordIndex][3])
        image[y:y+10,x:x+10] = (255,0,0)
        # image[ymax:ymax + 10, xmin:xmin + 10] = (0, 255, 0)
        # image[ymin:ymin + 10, xmax:xmax + 10] = (0, 255, 0)
        # image[ymax:ymax + 10, xmax:xmax + 10] = (0, 255, 0)
        # image[xmin:xmin+10,ymin:ymin+10] = (0,255,0)
        # image[xmin:xmin + 10,ymax:ymax + 10 ] = (0, 255, 0)
        # image[xmax:xmax + 10, ymin:ymin + 10 ] = (0, 255, 0)
        # image[xmax:xmax + 10,ymax:ymax + 10] = (0, 255, 0)
        newImage = image
        cv2.imshow("img", newImage)
        cv2.waitKey(-1)
        newFileName = "".join((images_dir, "\\plotted_predictions\\", filename))
        cv2.imwrite(newFileName, newImage)
        coordIndex += 1
    else:
        continue