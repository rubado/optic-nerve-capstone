from __future__ import absolute_import, division, print_function, unicode_literals

import os
import cv2
import argparse
import tensorflow as tf
import numpy as np
import datetime
import pandas as pd
import re
from tensorflow import keras
from tensorflow.keras.callbacks import ModelCheckpoint
from tensorflow.keras import datasets, layers, models
from tensorflow.keras.metrics import MeanSquaredError

def process_images(images_path, filename_path, convertToGray = False):
    images = []
    directory = images_path
    filenames = open(filename_path)
    filenames = filenames.readlines()

    for file in filenames:
        file = file.rstrip()
        full_path = "".join((directory, "\\", file))
        if file.endswith(".png"):
            img = cv2.imread(full_path,flags=cv2.IMREAD_UNCHANGED)

            if not convertToGray:
                images.append(img)
            else:
                gray_image = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
                images.append(gray_image)

            #
            # cv2.namedWindow("images")
            # while True:
            #     # display the image and wait for a keypress
            #     # cv2.imshow("images", cv2.imread(full_path))
            #     key = cv2.waitKey(1) & 0xFF
            #     # if the 'c' key is pressed, save and break from the loop
            #     if key == ord("c"):
            #         break
        else:
            continue

    images = np.asarray(images)

    return images


class CustomLoss(layers.Layer):

    def __init__(self):
        super(CustomLoss, self).__init__()
        self.w = self.add_weight(name = "my_weight",
                                 shape=(2,32),
                                 initializer='random_normal',
                                 trainable=True)
    def call(self, inputs):
        float_indices = inputs[1]
        indices = inputs[1]
        gradient_img = inputs[0]
        mult_tensor = tf.convert_to_tensor(10000, tf.float32)
        indices = tf.dtypes.cast(indices,dtype = tf.int32)

        losses = tf.gather_nd(gradient_img,indices,batch_dims = 1)


        losses = tf.math.multiply(losses, mult_tensor)



        self.add_loss(tf.reduce_mean(losses))
        return float_indices

ap = argparse.ArgumentParser()

ap.add_argument("-i", "--images", required=True, help="Path to the images")
ap.add_argument("-in", "--image_names", required=True, help="Path to the text file containing image filenames")
# ap.add_argument("-li", "--loss_images", required=True, help="Path to the loss images")
ap.add_argument("-x", "--labels", required=True, help="Path to the excel file containing labels created by using coords.py")
ap.add_argument("-c", "--checkpoint", required=True, help="Path to directory for saving best checkpoint during training")

args = vars(ap.parse_args())

images_directory = args["images"]
# loss_images_directory = args["loss_images"]
labels = args["labels"]
image_names = args["image_names"]
checkpoint_path = args["checkpoint"]




train_data = pd.read_csv(labels,delimiter=',')
train_data = train_data.set_index('file')
ordered_train_data = np.empty((0,2))

filenames = open(image_names)
filenames = filenames.readlines()


for file in filenames:
    file = file.rstrip()
    y = train_data.loc[file,'y']
    x = train_data.loc[file,'x']
    coordinates = np.asarray([[y,x]])

    ordered_train_data = np.append(ordered_train_data,coordinates,axis=0)


train_images = process_images(images_directory,image_names)
# loss_images = process_images(loss_images_directory,image_names,convertToGray = True)


train_images, loss_images = train_images / 255.0, loss_images/255.0

input_images = keras.Input(shape=(584,584,3))
loss_matrices = keras.Input(shape=(584,584))

mse = MeanSquaredError(name = 'mse')
my_loss_layer = CustomLoss()

x = layers.Conv2D(32,(3,3),activation = 'relu')(input_images)
x = layers.MaxPooling2D(2,2)(x)
x = layers.Conv2D(32,(3,3),activation = 'relu')(x)
x = layers.MaxPooling2D(2,2)(x)
x = layers.Flatten()(x)
x = layers.Dense(64,activation='relu')(x)
x = layers.Dense(64,activation='relu')(x)
x = layers.Dense(64,activation='relu')(x)

outputs = layers.Dense(2,)(x)
# outputs = my_loss_layer((loss_matrices,outputs))

model = keras.Model(inputs=[input_images], outputs=[outputs])

model.summary()





def loss(y_true,y_pred):
    mult_tensor = tf.convert_to_tensor(1, tf.float32)
    return tf.math.multiply(tf.math.reduce_mean(tf.math.square(tf.math.subtract(y_true,y_pred))),mult_tensor);


model.compile(optimizer='adam',
              loss = 'mse',
              metrics=['mse'])

log_dir="logs\\scalars\\" + datetime.datetime.now().strftime("%Y%m%d-%H%M%S")
tensorboard_callback = tf.keras.callbacks.TensorBoard(log_dir=log_dir, histogram_freq=1)
checkpointer = ModelCheckpoint(filepath=checkpoint_path, verbose=1, save_best_only=True)
history = model.fit([train_images],ordered_train_data, epochs=250, batch_size=32,shuffle = True, callbacks=[checkpointer,tensorboard_callback],validation_split = 0.06)
